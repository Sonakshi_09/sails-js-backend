/**
 * UserController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */


//var bcrypt = require('bcrypt');
var crypto = require('crypto');

/**
@description :: API for registeration
@Request_type :: POST
*/
module.exports = {
  register: function(req, res){
    if(!req.body.firstname || !req.body.email | !req.body.lastname){
      res.sendStatus(400);
    }
    var key = crypto.createCipher('aes-128-cbc', 'mypassword');
    var encrypted = key.update(req.body.password, 'utf8', 'hex');
    encrypted += key.final('hex');

    //console.log(mystr);

    data = {
      firstname: req.body.firstname,
      lastname: req.body.lastname,
      email: req.body.email,
      password: encrypted,
      dob: req.body.dob,
      role: req.body.role,
    };
    // console.log(data);
    //console.log(createdUser);
    User.create(data)
    .fetch()
    .exec((err,result) => {
      console.log(result);
      if(err){return res.serverError(err);}
      else{res.send(200,result);}
    });
  },


  /**
@description :: API for LOGIN
@Request_type :: POST
@Route :: localhost/login
*/
  login: function(req, res){

   

    data = {
      email: req.body.email,
      password: req.body.password,
    };
    console.log(data);
    
    User.findOne({email: req.body.email})
    .then(users => {
      console.log(users);

      var key = crypto.createCipher('aes-128-cbc', 'mypassword');
      var encrypted = key.update(req.body.password, 'utf8', 'hex');
      encrypted += key.final('hex');
      console.log(encrypted);

      if(!users){
        res.send('YOU ARE WRONG');
      }
      else if(encrypted === users.password){
        res.send(users);
      }
      else{
        return res.sendStatus(400);
      }
    });

  },

  /**
@description :: API for User fetching
@Request_type :: POST
*/

  fetch: function(req,res){
    //console.log(req.params.firstname);
    User.find({firstname: req.body.firstname, email:req.body.email })
        .then(result => {
          if (result) {
            console.log(result);
            res.send(result);
          } else {
            res.send(500);
          }
        });

  },
  /**
@description :: API for fetching single user details by email
@Request_type :: POST
*/
  fetchsingleuser: function(req,res){
    //console.log(req.params.firstname);
    User.findOne({email:req.body.email })
        .then(result => {
          if (result) {
            console.log(result);
            res.send(result);
          } else {
            res.send(500);
          }
        });

  },

  /**
@description :: API for deleting user by id
@Request_type :: DELETE
*/
  // userdeletebyID: function(req, res){
  //   User.destroy({
  //     id: req.params.id
  //   }).then(users => {
  //     console.log(users);
  //     res.ok(users);
  //   }).catch(err => res.serverError(err));
  // },
  userdeletebyID: function(req, res){
    console.log(req.params.id);
    User.destroy({where:{id: req.params.id}}).
    fetch()
    .exec((err,users) => {
      console.log(users);
      if(err)
      {return(err);}

      return res.json(users);
    });
  },
  /**
@description :: API for User fetching
@Request_type :: POST
*/
  userupdate: function(req,res)
  {
    if(req.body.email){
      var key = crypto.createCipher('aes-128-cbc', 'mypassword');
      var encrypted = key.update(req.body.password, 'utf8', 'hex');
      encrypted += key.final('hex');
      // console.log(encrypted);
      req.body.password = encrypted;
      User.update({email: req.body.email}, req.body)
      .fetch()
    .then((users) => {
      // console.log('Displaying Users');
      // console.log(users);
      if(!users){
        res.send('YOU ARE WRONG');
      }
      else {
        // users.password = encrypted;
        return res.json(users);
      }
    })
    .catch((err) => {
      console.log(err);
    });
    }}
};





//---------------------------------PRACTICE CODE --------------------------------


//   authenticate: async (request, response) => {
//     // Sign up user
//     if(request.body.action === 'signup') {
//     // Validate signup form
//     // Check if email is registered
//     // Create new user
// }

