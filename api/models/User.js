/**
 * User.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

// eslint-disable-next-line no-unused-vars
var bcrypt = require('bcrypt');

module.exports = {

  attributes: {
    firstname :{
      type: 'String',
      required: true
    },
    lastname :{
      type: 'String',
      required: false
    },
    email :{
      type: 'String',
      required: true,
      unique: true
    },
    password :{
      type: 'String',
      required: true,
    },
    dob: {
      type: 'String',
      required: false
    },
    role:{
      type: 'String',
      required: true

    },
  },
};


//   customToJSON: function() {
//     return _.omit(this, ['password']);
//   },

//   beforeCreate: function(user, cb){
//     bcrypt.genSalt(10, (_err,salt) => {
//       bcrypt.hash(user.password, salt, (err, hash) => {
//         // eslint-disable-next-line callback-return
//         if(err){cb(err);}
//         else{
//           user.password = hash;
//           // eslint-disable-next-line callback-return
//           cb();
//         }
//       });
//     });
//   }
// };




//  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
//  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
//  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝


//  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
//  ║╣ ║║║╠╩╗║╣  ║║╚═╗
//  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝


//  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
//  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
//  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝
